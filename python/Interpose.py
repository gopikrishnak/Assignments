list1 = [1,2,3,4]
interpose = ","
list2 = []
def interleave_func(list1):
    for i in range (len(list1)):
        if i < len(list1)-1:
            list2.append(list1[i])
            list2.append(interpose)
        else:
            list2.append(list1[i])
    return list2
x = interleave_func(list1)
print(x)
