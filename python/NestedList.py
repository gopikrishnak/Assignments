t = [[1, 2], [3], [4, 5, 6]]
def nested_sum(t):
    count =  0;
    for i in range(len(t)):
        for j in range(len(t[i])):
            count = count + t[i][j]
    return count       
count = nested_sum(t)
print(count)
