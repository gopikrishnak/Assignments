import turtle as tt
import math
bob = tt.Turtle()

def square(t, length):
    for i in range(4):
        t.fd(length)
        t.lt(90)
square(bob, 100)

def polygon(t, n, length):
    angle = 360 / n
    for i in range(n):
        t.fd(length)
        t.lt(angle)
polygon(bob, 8, 70)

def circle(t, r):
    circumference = 2 * math.pi * r
    print(math.pi)
    print(circumference)
    n = int(circumference / 3) + 1
    print(n)
    length = circumference / n
    print(length)
    polygon(t, n, length)
circle(bob,40)

def arc(t, r, angle):
    arc_length = 2 * math.pi * r * angle / 360
    n = int(arc_length / 3) + 1
    step_length = arc_length / n
    step_angle = angle / n
    for i in range(n):
        t.fd(step_length)
        t.lt(step_angle)
arc(bob,50,100)


def polyline(t, n, length, angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)


def polygon(t, n, length):
    angle = 360.0 / n
    polyline(t, n, length, angle)
polygon(bob,15,50)


def arc(t, r, angle):
    arc_length = 2 * math.pi * r * angle / 360
    n = int(arc_length / 3) + 1
    step_length = arc_length / n
    step_angle = float(angle) / n
    polyline(t, n, step_length, step_angle)

arc(bob,50,100)


def circle(t, r):
    arc(t, r, 360)
circle(bob,50)





        
