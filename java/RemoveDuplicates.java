package gopi.work;

import java.util.ArrayList;

public class RemoveDuplicates {
	
	public static ArrayList<Integer> removeDuplicates(int[] a) {
	
		int size = a.length;
		ArrayList<Integer> list = new ArrayList<Integer>();
		
	    for(int i=0;i<size ;i++) {
	    	
	    	if(i == size-1) {
	    		list.add(a[i]);
	    		break;
	    	}
	    	if( a[i] != a[i+1] ) {
	    		list.add(a[i]);
	       }
	    }
	    
        	
		return list;
		
		
		
	}

	public static void main(String[] args) {
		int[] a = {1,2,3,3,3,3,4,7,7,8};
		ArrayList<Integer> list = RemoveDuplicates.removeDuplicates(a);
			System.out.println(list);
	}
}
