package gopi.work;

public class LinkedList {
	private Node first;
	private Node end;
	private int length;

	public void insertFirst(int data) {
		Node n = new Node(data, null);
		first = n;
		end = first;
	}

	public void insertAfter(Node after, int data) {
		Node n = new Node(data, after);
		n.setAfter(first);
		first = n;
		length++;
	}

	public void removeFirst() {
		first = first.getAfter();
		length--;
		return;
	}

	public void removeAfter(Node after, int data) {

		Node temp = after.getAfter();
		after.setAfter(temp.getAfter());
		length--;
	}

	public static void main(String[] args) {
		LinkedList list = new LinkedList();
		list.insertFirst(10);
		list.insertAfter(list.first.getAfter(), 20);
		list.insertAfter(list.first.getAfter(), 30);
		list.insertAfter(list.first.getAfter(), 40);
		list.insertAfter(list.first.getAfter(), 50);
		System.out.println(list);
		list.removeFirst();
		System.out.println(list);
		list.removeAfter(list.first.getAfter(), list.first.getData());
		System.out.println(list);
	}

	@Override
	public String toString() {
		return "LinkedList [first=" + first + ", end=" + end + ", length=" + length + "]";
	}

}
